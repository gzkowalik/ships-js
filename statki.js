var view = {
	displayMessage: function(msg) {
		var messageArea = document.getElementById("messageArea");
		messageArea.innerHTML = msg;
	},

	displayHit: function(location) {
		var cell = document.getElementById(location);
		cell.setAttribute("class", "hit");
	},

	displayMiss: function(location) {
		var cell = document.getElementById(location);
		cell.setAttribute("class", "miss");
	}

}; 
function GameOver()
{
	var cell = document.getElementById("game");
	cell.setAttribute("class", "gameover");
}

var model = {
	boardSize: 7,
	numShips: 3,
	shipLength: 3,
	shipsSunk: 0,
	ships: [
		{ locations: ["", "", ""], hits: ["", "", ""] },
		{ locations: ["", "", ""], hits: ["", "", ""] },
		{ locations: ["", "", ""], hits: ["", "", ""] }
	],
	fire: function(guess){
		for (var i = 0; i < this.numShips; i++){
			var ship = this.ships[i];
			var index = ship.locations.indexOf(guess);
			if (index >= 0)
			{
				if (ship.hits[index] == "")
				{
					ship.hits[index] = "hit";
					view.displayHit(guess);
					view.displayMessage("TRAFIONY");
					
					
					
					if (this.isSunk(ship))
					{
						this.shipsSunk++;
						view.displayMessage("TRAFIONY-ZATOPIONY");
					}
					return true;
				}
				else
				{
					alert("Ten okręt już trafiłeś w tym miejscu.");
					return false;
				}
				
			}
		}
		view.displayMiss(guess);
		view.displayMessage("PUDŁO");
		return false;
	},
	isSunk: function(ship) {
		for (var i = 0; i < this.shipLength; i++)  {
			if (ship.hits[i] != "hit")
				return false
		}
		return true;
	},
	generateShip: function() {
		var direction = Math.floor(Math.random() * 2);
		if (direction == 0)
		{
			//poziom
			var row = Math.floor(Math.random() * this.boardSize);
			var col = Math.floor(Math.random() * (this.boardSize - this.shipLength));
		}
		else
		{
			//pion
			var col = Math.floor(Math.random() * this.boardSize);
			var row = Math.floor(Math.random() * (this.boardSize - this.shipLength));
		}
		var newShipslocations = [];
		for (var i = 0; i < this.shipLength; i++)
		{
			console.log("Wspolrzedna: " + i);
			if (direction == 0)
			{
				newShipslocations.push(row + "" + (col + i));
			}
			else
			{
				newShipslocations.push((row + i) + "" + col);
			}
			console.log(newShipslocations);
		}
		
		return newShipslocations;
	},
	
	collision: function(locations) {
		for (var i = 0; i < this.numShips; i++)
		{
			var ship = model.ships[i];
			for (var j = 0; j < locations.length; j++)
			{
				if (ship.locations.indexOf(locations[j]) >= 0)
					return true;
			}
		}
		return false;
	},
	generateShipLocations: function(){
		var locations;
		for (var i = 0; i < this.numShips; i++)
		{
			do {
				console.log("łódka: " + i);
				locations = this.generateShip();
			} while(this.collision(locations));
			console.log(i + locations);
			this.ships[i].locations = locations;
			
		}
	}
	
};
function parseGuess(guess) {
		if (guess === null || guess.length !== 2)
			alert("Proszę podać prawidłowe dane.")
		else
		{
			var alphabet = [ "A", "B", "C", "D", "E", "F", "G"];
			firstChar = guess.charAt(0);
			row = alphabet.indexOf(firstChar);
			column = guess.charAt(1);
			if (isNaN(row) || isNaN(column))
				alert("To nie są współrzędne.")
			else if (row < 0 || row >= model.boardSize || column < 0 || column >= model.boardSize)
				alert("Pole poza planszą.")
			else
				return row+column;	
		}
	}
var controller = {
	guesses: 0,
	processGuess: function(guess) {
		var location = parseGuess(guess);
		if (location)
		{
			this.guesses++;
			hit = model.fire(location);
			if (hit && model.shipsSunk === model.numShips)
			{
				console.log(model.shipsSunk + "-zatopionych a wszystkich-" + model.numShips);
				view.displayMessage("Zatopiłeś wszystkie okręty w " + this.guesses + " próbach.");
				setTimeout(GameOver, 3000);
				setTimeout("window.location='http://student.agh.edu.pl/milqaa/statki.php'", 5000);
				
			}
				
		}
	}
}
function handleFireButton() {
	var guessInput = document.getElementById("guessInput");
	var guess = guessInput.value.toUpperCase();

	controller.processGuess(guess);

	guessInput.value = "";
}
function handleKeyPress(e) {
	var fireButton = document.getElementById("fireButton");

	e = e || window.event;

	if (e.keyCode === 13) {
		fireButton.click();
		return false;
	}
}
window.onload = init;

function init() {
	var fireButton = document.getElementById("fireButton");
	fireButton.onclick = handleFireButton;

	var guessInput = document.getElementById("guessInput");
	guessInput.onkeypress = handleKeyPress;
	model.generateShipLocations();
}

